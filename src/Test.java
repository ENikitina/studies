import java.util.HashMap;
import java.util.Random;

public class Test {
    public static void main(String[] args){
        int[] randomBlock = new int[100];
        Random rand = new Random();
        for (int i = 0; i < randomBlock.length; i++) {
            randomBlock[i] = rand.nextInt(10);
        }

        HashMap<Integer, Integer> countNumber = new HashMap<>();
        for (int n : randomBlock) {
            if (!countNumber.containsKey(n)) {
                countNumber.put(n, 1);
            }
            if (countNumber.containsKey(n)){
                int increaseCount = 0;
                increaseCount = countNumber.get(n) +1;
                countNumber.remove(n);
                countNumber.put(n, increaseCount);
            }

        }

        System.out.println(countNumber);
    }
}